'use strict';

/*
    This file can be used as entry point for webpack!
 */
const lightbox2 = require('lightbox2');
const stickyTable = require('sticky-table-headers');

$('#PIT').popover({ content: 'Cпециализированная больничная палата для лечения пациентов, находящихся в остром, опасном для жизни состоянии. Предназначена для кратковременной поддержки серьезно больных или раненых пациентов, чье состояние считается не безнадежным.',
    title: 'Палата Интенсивной Терапии',
    placement: 'top',
    trigger: 'hover'});

$("table").stickyTableHeaders();