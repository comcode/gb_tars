let data = {docslist: {
    aside: true,
    therapists: {
        aside: true,
        h2block: 'Врачи терапевты и педиатры',
        list: [
            {
                name: 'Емельянова Ольга Сергеевна',
                description: '',
                imgUrl: 'https://picsum.photos/350/220/?random&blur',
                position: 'врач терапевт участковый',
                diploma: {
                    year: '1998',
                    name: 'Московский медицинский стоматологический институт',
                    specialty: 'лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'терапия',
                    from: '2016',
                    to: '2021'
                },
                category: {
                    type: 'без',
                    to: ''
                }
            }, {
                name: 'Редькина Екатерина Александровна',
                description: '',
                imgUrl: 'https://picsum.photos/350/220/?random&blur',
                position: 'врач терапевт участковый',
                diploma: {
                    year: '2013',
                    name: 'Ивановская государственная медицинская академия',
                    specialty: 'лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'терапия',
                    from: '2013',
                    to: '2018'
                },
                category: {
                    type: 'без',
                    to: ''
                }
            }, {
                name: 'Рукшина Татьяна Юрьевна',
                description: '',
                imgUrl: 'https://picsum.photos/350/220/?random&blur',
                position: 'врач терапевт участковый',
                diploma: {
                    year: '2009',
                    name: 'Ивановская государственная медицинская академия',
                    specialty: 'лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'терапия',
                    from: '2014',
                    to: '2019'
                },
                category: {
                    type: 'без',
                    to: ''
                }
            }, {
                name: 'Солдатова Ксения Михайловна',
                description: '',
                imgUrl: 'https://picsum.photos/350/220/?random&blur',
                position: 'врач терапевт участковый',
                diploma: {
                    year: '2009',
                    name: 'Ивановская государственная медицинская академия',
                    specialty: 'лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'терапия',
                    from: '2014',
                    to: '2019'
                },
                category: {
                    type: 'без',
                    to: ''
                }
            }, {
                name: 'Камышан Елена Гениевна',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'заведующий педиатрическим отделением, врач педиатр участковый',
                diploma: {
                    year: '1984',
                    name: 'Ивановский государственный медицинский институт им А.С. Бубнова',
                    specialty: 'педиатрия',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'педиатрия',
                    from: '2013',
                    to: '2018'
                },
                category: {
                    type: 'высшей',
                    to: '2019'
                }
            }, {
                name: 'Кустова Елена Вадимовна',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'врач педиатр участковый',
                diploma: {
                    year: '1977',
                    name: 'Ивановский государственный медицинский институт',
                    specialty: 'педиатрия',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'педиатрия',
                    from: '2016',
                    to: '2021'
                },
                category: {
                    type: 'первой',
                    to: '2022'
                }
            }, {
                name: 'Цыбанова Нина Федоровна',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'врач педиатр участковый',
                diploma: {
                    year: '1977',
                    name: 'Ивановский государственный медицинский институт',
                    specialty: 'педиатрия',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'педиатрия',
                    from: '2017',
                    to: '2022'
                },
                category: {
                    type: 'первой',
                    to: '2020'
                }
            }, {
                name: 'Шулепникова Людмила Вадимовна',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'врач педиатр участковый',
                diploma: {
                    year: '1975',
                    name: 'Ивановский государственный медицинский институт им А.С. Бубнова',
                    specialty: 'педиатрия',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'педиатрия',
                    from: '2016',
                    to: '2021'
                },
                category: {
                    type: 'первой',
                    to: '2021'
                }
            }
        ]},
    commons: {
        h2block: 'Врачи узких специализаций',
        list: [
            {
                name: 'Носыч Артём Вячеславович',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'врач невролог',
                diploma: {
                    year: '2013',
                    name: 'Ивановская государственная медицинская академия',
                    specialty: 'Лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'неврология',
                    from: '2014',
                    to: '2019'
                },
                category: {
                    type: 'без',
                    to: ''
                }
            }, {
                name: 'Никифорова Майя Александровна',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'врач невролог',
                diploma: {
                    year: '1998',
                    name: 'Ивановская государственная медицинская академия',
                    specialty: 'Лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'неврология',
                    from: '2016',
                    to: '2021'
                },
                category: {
                    type: 'первой',
                    to: '2018'
                }
            }, {
                name: 'Ежков Александр Юрьевич',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'врач невролог',
                diploma: {
                    year: '2005',
                    name: 'Ярославская государственная медицинская академия ',
                    specialty: 'Педиатрия',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'неврология',
                    from: '2017',
                    to: '2022'
                },
                category: {
                    type: 'первой',
                    to: '2022'
                }
            }, {
                name: 'Лебедев Рудольф Владимирович',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'врач уролог',
                diploma: {
                    year: '1995',
                    name: 'Тверской государсмтвенной медицинской академией',
                    specialty: 'Лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'урология',
                    from: '2015',
                    to: '2020'
                },
                category: {
                    type: 'без',
                    to: ''
                }
            }, {
                name: 'Рыбина Ирина Вадимовна',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'врач дерматовенеролог',
                diploma: {
                    year: '1995',
                    name: 'Ивановский государственный медицинский институт им. А.С. Бубнова',
                    specialty: 'Лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'дерматовенерология',
                    from: '2017',
                    to: '2022'
                },
                category: {
                    type: 'первой',
                    to: '2020'
                }
            }, {
                name: 'Новикова Валентиа Александровна',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'врач отоларинголог',
                diploma: {
                    year: '1976',
                    name: 'Таджикский государственный медицинский институт им. Абу-Али ибн Сино',
                    specialty: 'Лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'отоларингология',
                    from: '2014',
                    to: '2019'
                },
                category: {
                    type: 'первой',
                    to: '2020'
                }
            }, {
                name: 'Коновалова Светлана Викторовна',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'врач эндокринолог',
                diploma: {
                    year: '2000',
                    name: 'Ивановская государственная медицинская академия ',
                    specialty: 'Лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'энокринология',
                    from: '2013',
                    to: '2018'
                },
                category: {
                    type: 'высшей',
                    to: '2020'
                }
            }, {
                name: 'Ипполитова Марина Николаевна',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'врач офтальмолог',
                diploma: {
                    year: '2009',
                    name: 'Мордовский государственный университет',
                    specialty: 'Лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'офтальмология',
                    from: '2015',
                    to: '2020'
                },
                category: {
                    type: 'второй',
                    to: '2022'
                }
            }, {
                name: 'Солодушенков Сергей Борисович',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'врач функциональной диагностики',
                diploma: {
                    year: '1998',
                    name: 'Ивановский государственный медицинский институт им. А.С. Бубнова',
                    specialty: 'Лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'трансфузиология',
                    from: '2015',
                    to: '2020'
                },
                category: {
                    type: 'высшей',
                    to: '2018'
                }
            }, {
                name: 'Лопунова Елена Владимировна',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'главный врач, пульмонолог',
                diploma: {
                    year: '2000',
                    name: 'Ивановская государственная медицинская академия',
                    specialty: 'Лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'пульмонология',
                    from: '2014',
                    to: '2019'
                },
                category: {
                    type: 'высшей',
                    to: '2021'
                }
            }, {
                name: 'Чеснокова Людмила Валентиновна',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'врач акушер-гинеколог',
                diploma: {
                    year: '2005',
                    name: 'Нижегородская государственная медицинская академия',
                    specialty: 'Лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'акушерство и геникология',
                    from: '2016',
                    to: '2021'
                },
                category: {
                    type: 'второй',
                    to: '2019'
                }
            }, {
                name: 'Ермилова Наталья Леонидовна',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'врач акушер-гинеколог',
                diploma: {
                    year: '2002',
                    name: 'Ивановская государственная медицинская академия',
                    specialty: 'Лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'акушерство и геникология',
                    from: '2013',
                    to: '2018'
                },
                category: {
                    type: 'без',
                    to: '2022'
                }
            }, {
                name: 'Гончаренко Елена Анатольевна',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'врач онколог',
                diploma: {
                    year: '2006',
                    name: 'Ивановская государственная медицинская академия',
                    specialty: 'Лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'онкология',
                    from: '2013',
                    to: '2018'
                },
                category: {
                    type: 'первой',
                    to: '2020'
                }
            }, {
                name: 'Солодянкин Андрей Валентинович',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'врач психиатр-нарколог',
                diploma: {
                    year: '1998',
                    name: 'Ивановская государственная медицинская академия',
                    specialty: 'Лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'психиатрия',
                    from: '2013',
                    to: '2020'
                },
                category: {
                    type: 'первой',
                    to: '2022'
                }
            }
        ]
    },
    hospital: {
        h2block: 'Врачи стационара',
        list: [
            {
                name: 'Будина Ольга Васильевна',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'врач анестезиолог-реаниматолог',
                diploma: {
                    year: '2006',
                    name: 'Ивановская государственная медицинская академия',
                    specialty: 'Лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'анестезиология и реаниматология',
                    from: '2013',
                    to: '2018'
                },
                category: {
                    type: 'второй',
                    to: '2021'
                }
            }, {
                name: 'Каталеев Дмитрий Юрьевич',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'врач анестезиолог-реаниматолог',
                diploma: {
                    year: '2005',
                    name: 'Ивановская государственная медицинская академия',
                    specialty: 'Лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'трансфузиология',
                    from: '2015',
                    to: '2020'
                },
                category: {
                    type: 'первой',
                    to: '2020'
                }
            }, {
                name: 'Доровских Сергей Владимирович',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'заведующий терапевтическим отделением, врач терапевт',
                diploma: {
                    year: '2004',
                    name: 'Ивановская государственная медицинская академия',
                    specialty: 'Лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'терапия',
                    from: '2016',
                    to: '2021'
                },
                category: {
                    type: 'первой',
                    to: '2020'
                }
            }, {
                name: 'Солодушенков Сергей Борисович',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'врач анестезиолог-реаниматолог',
                diploma: {
                    year: '1998',
                    name: 'Ивановский государственный медицинский институт им. А.С. Бубнова',
                    specialty: 'Лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'трансфузиология',
                    from: '2015',
                    to: '2020'
                },
                category: {
                    type: 'высшей',
                    to: '2018'
                }
            }, {
                name: 'Носыч Артём Вячеславович',
                description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean ultricies lacus non rhoncus consequat. Aliquam.',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'заведующий неврологическим отделение, врач невролог',
                diploma: {
                    year: '2013',
                    name: 'Ивановская государственная медицинская академия',
                    specialty: 'Лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'неврология',
                    from: '2014',
                    to: '2019'
                },
                category: {
                    type: 'без',
                    to: ''
                }
            }
        ]
    },
    ambulance: {
        h2block: 'Врачи и фельдшеры отделения скорой помощи',
        list: [
            {
                name: 'Алексеева Лариса Николаевна',
                description: '',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'фельдшер скорой медицинской помощи',
                diploma: {
                    year: '1986',
                    name: 'Владимирский базовый медицинский колледж',
                    specialty: 'Лечебное дело',
                    qualification: 'фельдшер'
                },
                certificate: {
                    specialty: 'скорая и неотложная помощь',
                    from: '2013',
                    to: '2018'
                },
                category: {
                    type: 'высшей',
                    to: '2018'
                }
            }, {
                name: 'Андреева Наталья Сергеевна',
                description: '',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'фельдшер скорой медицинской помощи',
                diploma: {
                    year: '2002',
                    name: 'Владимирский базовый медицинский колледж',
                    specialty: 'Лечебное дело',
                    qualification: 'фельдшер'
                },
                certificate: {
                    specialty: 'скорая и неотложная помощь',
                    from: '2016',
                    to: '2021'
                },
                category: {
                    type: 'первой',
                    to: '2019'
                }
            }, {
                name: 'Анисичкина Светлана Васильевна',
                description: '',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'фельдшер скорой медицинской помощи',
                diploma: {
                    year: '2000',
                    name: 'Владимирский базовый медицинский колледж',
                    specialty: 'Лечебное дело',
                    qualification: 'фельдшер'
                },
                certificate: {
                    specialty: 'скорая и неотложная помощь',
                    from: '2017',
                    to: '2022'
                },
                category: {
                    type: 'высшей',
                    to: '2019'
                }
            }, {
                name: 'Белогруд Вера Михайловна',
                description: '',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'медецинская сестра (фельдшер) по приему вызовов СМП',
                diploma: {
                    year: '1979',
                    name: 'Владимирский базовый медицинский колледж',
                    specialty: 'Лечебное дело',
                    qualification: 'фельдшер'
                },
                certificate: {
                    specialty: 'скорая и неотложная помощь',
                    from: '2013',
                    to: '2018'
                },
                category: {
                    type: 'высшей',
                    to: '2019'
                }
            }, {
                name: 'Богатова Раиса Петровна',
                description: '',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'фельдшер скорой медицинской помощи',
                diploma: {
                    year: '1974',
                    name: 'Волгоградское медицинское училище областное № 4',
                    specialty: 'Лечебное дело',
                    qualification: 'фельдшер'
                },
                certificate: {
                    specialty: 'скорая и неотложная помощь',
                    from: '2015',
                    to: '2020'
                },
                category: {
                    type: 'без',
                    to: ''
                }
            }, {
                name: 'Веселова Мария Викторовна',
                description: '',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'фельдшер скорой медицинской помощи',
                diploma: {
                    year: '2010',
                    name: 'Владимирский базовый медицинский колледж',
                    specialty: 'Лечебное дело',
                    qualification: 'фельдшер'
                },
                certificate: {
                    specialty: 'скорая и неотложная помощь',
                    from: '2014',
                    to: '2019'
                },
                category: {
                    type: 'второй',
                    to: '2019'
                }
            }, {
                name: 'Глебова Ирина Владимировна',
                description: '',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'фельдшер скорой медицинской помощи',
                diploma: {
                    year: '1986',
                    name: 'Владимирское медицинское училище',
                    specialty: 'Лечебное дело',
                    qualification: 'фельдшер'
                },
                certificate: {
                    specialty: 'скорая и неотложная помощь',
                    from: '2017',
                    to: '2022'
                },
                category: {
                    type: 'высшей',
                    to: '2018'
                }
            }, {
                name: 'Козлова Людмила Владимировна',
                description: '',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'медецинская сестра (фельдшер) по приему вызовов СМП',
                diploma: {
                    year: '2002',
                    name: 'Владимирское медицинское училище',
                    specialty: 'Лечебное дело',
                    qualification: 'фельдшер'
                },
                certificate: {
                    specialty: 'скорая и неотложная помощь',
                    from: '2015',
                    to: '2020'
                },
                category: {
                    type: 'высшей',
                    to: '2022'
                }
            }, {
                name: 'Коновалова Светлана Викторовна',
                description: '',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'врач скорой медицинской помощи',
                diploma: {
                    year: '2000',
                    name: 'Ивановская государственная медицинская академия',
                    specialty: 'Лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'скорая медицинская помощь',
                    from: '2013',
                    to: '2018'
                },
                category: {
                    type: 'высшей',
                    to: '2020'
                }
            }, {
                name: 'Коновалова Галина Владимировна',
                description: '',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'заведующий – врач СМП',
                diploma: {
                    year: '1985',
                    name: 'Ивановский государственный медицинский институт им. А.С. Бубнова',
                    specialty: 'Лечебное дело',
                    qualification: 'врач'
                },
                certificate: {
                    specialty: 'скорая медицинская помощь',
                    from: '2013',
                    to: '2018'
                },
                category: {
                    type: 'высшей',
                    to: '2019'
                }
            }, {
                name: 'Маркевич Анна Николаевна',
                description: '',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'фельдшер скорой медицинской помощи',
                diploma: {
                    year: '2000',
                    name: 'Владимирский базовый медицинский колледж',
                    specialty: 'Лечебное дело',
                    qualification: 'фельдшер'
                },
                certificate: {
                    specialty: 'скорая медицинская помощь',
                    from: '2016',
                    to: '2021'
                },
                category: {
                    type: 'высшей',
                    to: '2018'
                }
            }, {
                name: 'Пешева Галина Ивановна',
                description: '',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'медецинская сестра (фельдшер) по приему вызовов СМП',
                diploma: {
                    year: '',
                    name: ' ',
                    specialty: 'Лечебное дело',
                    qualification: 'фельдшер'
                },
                certificate: {
                    specialty: 'скорая и неотложная помощь',
                    from: '2017',
                    to: '2022'
                },
                category: {
                    type: 'высшей',
                    to: '2018'
                }
            }, {
                name: 'Пискунова Лариса Аркадьевна',
                description: '',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'фельдшер скорой медицинской помощи',
                diploma: {
                    year: '',
                    name: '',
                    specialty: 'Лечебное дело',
                    qualification: 'фельдшер'
                },
                certificate: {
                    specialty: 'скорая медицинская помощь',
                    from: '2013',
                    to: '2018'
                },
                category: {
                    type: 'без',
                    to: '2018'
                }
            }, {
                name: 'Федотова Ирина Викторовна',
                description: '',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'фельдшер скорой медицинской помощи',
                diploma: {
                    year: '2008',
                    name: 'Владимирский базовый медицинский колледж',
                    specialty: 'Лечебное дело',
                    qualification: 'фельдшер'
                },
                certificate: {
                    specialty: 'скорая медицинская помощь',
                    from: '2013',
                    to: '2018'
                },
                category: {
                    type: 'первой',
                    to: '2018'
                }
            }, {
                name: 'Филатова Юлия Александровна',
                description: '',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'фельдшер скорой медицинской помощи',
                diploma: {
                    year: '2006',
                    name: 'Ковровское медучилище',
                    specialty: 'Лечебное дело',
                    qualification: 'фельдшер'
                },
                certificate: {
                    specialty: 'скорая медицинская помощь',
                    from: '2014',
                    to: '2019'
                },
                category: {
                    type: 'второй',
                    to: '2017'
                }
            }, {
                name: 'Цветкова Любовь Николаевна',
                description: '',
                imgUrl: 'https://picsum.photos/350/220/?random',
                position: 'медецинская сестра (фельдшер) по приему вызовов СМП',
                diploma: {
                    year: '1982',
                    name: 'Владимирское медицинское училище',
                    specialty: 'Лечебное дело',
                    qualification: 'фельдшер'
                },
                certificate: {
                    specialty: 'скорая и неотложная помощь',
                    from: '2017',
                    to: '2022'
                },
                category: {
                    type: 'без',
                    to: '2022'
                }
            },
        ]
    },
        stomatology: {
        h2block: 'Зубные врачи и врачи стоматологи',
            list: [
                {
                    name: 'Борисов Игорь Александрович',
                    description: '',
                    imgUrl: 'https://picsum.photos/350/220/?random',
                    position: 'врач стоматолог-ортопед',
                    diploma: {
                        year: '1982',
                        name: 'Калининский государственный медицинский институт',
                        specialty: 'стоматология',
                        qualification: 'врач'
                    },
                    certificate: {
                        specialty: 'стоматология ортопедическая',
                        from: '2016',
                        to: '2021'
                    },
                    category: {
                        type: 'без',
                        to: ''
                    }
                }, {
                    name: 'Веселов Андрей Михайлович',
                    description: '',
                    imgUrl: 'https://picsum.photos/350/220/?random',
                    position: 'зубной врач',
                    diploma: {
                        year: '2005',
                        name: 'Владимирский базовый медицинский колледж',
                        specialty: 'стоматология',
                        qualification: 'зубной врач'
                    },
                    certificate: {
                        specialty: 'стоматология',
                        from: '2015',
                        to: '2020'
                    },
                    category: {
                        type: 'первой',
                        to: '2020'
                    }
                }, {
                    name: 'Гаврилова Татьяна Александровна',
                    description: '',
                    imgUrl: 'https://picsum.photos/350/220/?random',
                    position: 'заведующий врач стоматолог терапевт, врач стоматолог детский',
                    diploma: {
                        year: '1992',
                        name: 'Ижевская государственная медицинская академия',
                        specialty: 'стоматология',
                        qualification: 'врач'
                    },
                    certificate: {
                        specialty: 'стоматология терапевтическая',
                        from: '2014',
                        to: '2019'
                    },
                    category: {
                        type: 'высшей',
                        to: '2019'
                    }
                }, {
                    name: 'Матяс Елизавета Леонидовна',
                    description: '',
                    imgUrl: 'https://picsum.photos/350/220/?random',
                    position: 'зубной врач',
                    diploma: {
                        year: '1986',
                        name: 'Владимирское медицинское училище',
                        specialty: 'стоматология',
                        qualification: 'зубной врач'
                    },
                    certificate: {
                        specialty: 'стоматология',
                        from: '2016',
                        to: '2021'
                    },
                    category: {
                        type: 'высшей',
                        to: '2021'
                    }
                }, {
                    name: 'Тузкова Анастасия Алексеевна',
                    description: '',
                    imgUrl: 'https://picsum.photos/350/220/?random',
                    position: 'зубной врач',
                    diploma: {
                        year: '2005',
                        name: 'Владимирский базовый медицинский колледж',
                        specialty: 'стоматология',
                        qualification: 'зубной врач'
                    },
                    certificate: {
                        specialty: 'стоматология',
                        from: '2014',
                        to: '2019'
                    },
                    category: {
                        type: 'без',
                        to: ''
                    }
                }, {
                    name: 'Чайкина Наталья Сергеевна',
                    description: '',
                    imgUrl: 'https://picsum.photos/350/220/?random',
                    position: 'зубной врач',
                    diploma: {
                        year: '1995',
                        name: 'Владимирский базовый медицинский колледж',
                        specialty: 'стоматология',
                        qualification: 'зубной врач'
                    },
                    certificate: {
                        specialty: 'стоматология',
                        from: '2017',
                        to: '2022'
                    },
                    category: {
                        type: 'первой',
                        to: '2020'
                    }
                },
            ]
    }
}};
